FROM registry.access.redhat.com/ubi7/ubi
COPY bin /work/
RUN chmod 775 /work
VOLUME ["/data/db"]
WORKDIR /data
EXPOSE 27017
CMD ["/work/mongod", "--bind_ip", "0.0.0.0"]
